package com.thathustudio.spage.exception;

/**
 * Created by Phung on 16/12/2016.
 */

public class SpageException extends Exception {
    public SpageException(String message, Throwable cause){
        super(message, cause);
    }
}
