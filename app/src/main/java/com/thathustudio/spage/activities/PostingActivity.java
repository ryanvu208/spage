package com.thathustudio.spage.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.thathustudio.spage.R;
import com.thathustudio.spage.model.Comment;


public class PostingActivity extends SpageActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getRootLayoutRes() {
        return R.layout.activity_posting;
    }

    private void createComment() {

    }

    private void updateComment(){

    }

    private void deleteComment(){

    }

}
